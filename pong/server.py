from http.server import HTTPServer, BaseHTTPRequestHandler


class PongHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write("pong".encode())


httpd = HTTPServer(("", 5000), PongHandler)
httpd.serve_forever()
