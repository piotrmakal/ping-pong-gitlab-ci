import os
import requests

from flask import Flask

app = Flask(__name__)


@app.route("/")
def ping():
    response = requests.get(os.environ.get("PONG_HOST"))
    return response.text
