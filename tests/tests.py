import os
import requests
import unittest


class AcceptanceTests(unittest.TestCase):

    def test_should_response_with_pong(self):
        response = requests.get(os.environ.get("PING_HOST"))

        self.assertEqual(response.text, "pong")
